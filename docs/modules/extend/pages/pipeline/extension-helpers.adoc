= Extension Helpers

The Pipeline object provides several utility methods that make writing extensions easier.
These utility methods are called [.term]*helpers* because they help you to write extensions.

== Access helpers

To use the helpers, you need a reference to the Pipeline object.
There are two ways to get the Pipeline object.
If the listener functions are defined within the `register` function for the extension, they can access the pipeline argument of the `register` function due to variable scoping.
Otherwise, the listener function can reference the Pipeline object using the standard `this` keyword.
The Pipeline object binds itself to the listener function when the listener function is registered.

== updateVars(Object)

The `updateVars` helper method provides a means for adding or replacing pipeline variables.
The method accepts a single argument of type Object, where the keys of the object are the variable names and the values are the variable values.
This method doesn't return a value.

If you want to remove a variable, specify the value as `undefined`.
Keep in mind that locked variables can't be replaced.

Here's an example that shows how to replace the `playbook` and `siteCatalog` variables from a listener:

[source,js]
----
playbook = JSON.parse(JSON.stringify(playbook))
siteCatalog = new Proxy(siteCatalog, {})
this.updateVars({ playbook, siteCatalog })
----

You can also use the `updateVars` method to introduce new variables into the pipeline.
The site generator will not recognize or use these variables.
However, other extensions, or listeners in the same extension, can use them.

[#halt]
== halt()

The `halt` helper method provides a way to halt the operation of the Antora site generator.
This method doesn't accept any arguments and doesn't return a value.

When called, if any messages are logged that exceed the xref:playbook:runtime-log-failure-level.adoc[failure level threshold], Antora will exit with a non-zero exit code.
Otherwise, Antora will exit with a zero exit code (i.e., successfully).

The `halt` helper is useful if you only need to run Antora partially and don't want to throw an error to make Antora stop.
You might use it for warming up a cache or performing reference validation.
Keep in mind that if you call `halt`, Antora won't publish a site.

Here's an example that shows how to halt Antora from a listener:

[source,js]
----
console.log('Our work is done here. Shut it down.')
this.halt()
----

== require(String)

The `require` helper method allows you to require libraries within the context of the Antora installation.
This method accepts a single argument of type String, which is a require request (i.e., the name of a Node.js module or a source file within a module).
This method returns the object that the specified module or source file exports.
If the request can't be resolved, the method throws an Error with code `MODULE_NOT_FOUND`.

From time to time when writing extensions, you may need to access code provided by Antora.
Examples include the logger, the ContentCatalog, or a utility function like `parseResourceId`.
This method allows you to require this code without having to declare a dependency on Antora.
That dependency is implicit since the extension runs within the context of of Antora.
This method provides a way to require that code.

Here's an example that shows how to get the version of the site generator that is currently running from a listener:

[source,js]
----
const { name, version } = this.require('@antora/site-generator-default/package')
console.log(`Running ${name} version ${version}`)
----

Since the extension is already running in the context of the site generator, here's a slightly simpler way to achieve the same result:

[source,js]
----
const { name, version } = this.require('../package')
console.log(`Running ${name} version ${version}`)
----

For a more practical example, you can use the `require` helper method to create a child logger for your extension.
Typically, you would do this in the `register` function, then access the same instance of the logger throughout your extension.

[source,js]
----
module.exports.register = (pipeline) => {
  const logger = pipeline.require('@antora/logger')('extension-name')
  pipeline.on('playbookBuilt', () => {
    logger.info('Let it be known. The playbook has been built!')
  })
}
----

You will see the following message in your terminal when you run Antora with this extension enabled:

[.output]
....
[12:24:37.731] INFO (extension-name): Let it be known. The playbook has been built!
....
